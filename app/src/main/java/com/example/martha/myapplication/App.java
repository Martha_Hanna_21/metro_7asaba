package com.example.martha.myapplication;
import com.google.android.gms.ads.MobileAds;
import android.app.Application;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MobileAds.initialize(this,
                "ca-app-pub-7543849616707016~1139336487");
    }
}
