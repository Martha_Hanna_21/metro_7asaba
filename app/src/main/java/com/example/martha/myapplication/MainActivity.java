package com.example.martha.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import android.view.View;
import android.content.Intent;




public class MainActivity extends AppCompatActivity {
    private AdView mAdView;
    private Spinner dropdown;
    private Spinner dropdown2;
    private Button button;
    private Toast toast;

   /* public onButtonClick(View v){
        v.putExtra();

    }*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        MobileAds.initialize(this,
//                "ca-app-pub-7543849616707016~1139336487");
        //get the spinner from the xml.
        Spinner dropdown =  findViewById(R.id.spinner1);
        Spinner dropdown2 =   findViewById(R.id.spinner2);



        //create a list of items for the spinner.
        String[] items = new String[]{
"اختار المحطة ...",
                "المرج الجديد",
                "المرج",
                "عزبة النخل",
                "عين شمس",
                "المطرية",
                "حلمية الزيتون",
                "حدايق الزيتون",
                "سراي القبة",
                "حمامات القبة",
                "كوبري القبة",
                "منشية الصدر",
                "الدمرداش",
                "غمرة",
                "الشهدا",
                "عرابي",
                "جمال عبد الناصر",
                "السادات",
                "سعد زغلول",
                "السيدة زينب",
                "الملك الصالح",
                "مارجرجس",
                "الزهراء",
                "دار السلام",
                "حدايق المعادي",
                "المعادي",
                "ثكنات المعادي",
                "طرة البلد",
                "كوتسيكا",
                "طرة الاسمنت",
                "المعصرة",
                "حدايق حلوان",
                "وادي حوف",
                "جامعة حلوان",
                "عين حلوان",
                "حلوان",

                "شبرا الخيمة",
                "كلية الزراعة",
                "المظلات",
                "الخلفاوي",
                "سانت تريز",
                "روض الفرج",
                "مسرة",
                "الشهدا",
                "العتبة",
                "محمد نجيب",
                "السادات",
                "الاوبرا",
                "الدقي",
                "البحوث",
                "جامعة القاهرة",
                "فيصل",
                "جيزة",
                "ام المصريين",
                "ساقية مكي",
                "المنيب" ,

                "العتبة" ,
                "باب الشعرية",
                "الجيش" ,
                "عبده باشا",
                "العباسية" ,
                "المعرض" ,
                "ستاد القاهرة",
                "كلية البنات" ,
                "الاهرام"};
        //create an adapter to describe how the items are displayed, adapters are used in several places in android.
        //There are multiple variations of this, but this is the basic variant.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);

        //set the spinners adapter to the previously created one.
        dropdown.setAdapter(adapter);

        dropdown2.setAdapter(adapter2);


        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
               // .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);

        //  dropdown.setSelection(adapter.getCount());
        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Spinner mySpinner = (Spinner) findViewById(R.id.spinner1);
                String from_station = mySpinner.getSelectedItem().toString();

                Spinner mySpinner2 = (Spinner) findViewById(R.id.spinner2);
                String to_station = mySpinner2.getSelectedItem().toString();

                if (from_station.equals(to_station) ||
                        from_station.equals(to_station) ||
                        to_station.equals("اختار المحطة ...")) {
                    Toast.makeText(MainActivity.this, "من فضلك راجع اختياراتك واتأكد ان المحطتين مش زي بعض",
                            Toast.LENGTH_LONG).show();
                } else {

                    // Code here executes on main thread after user presses button
                    Intent myIntent = new Intent(MainActivity.this, Result.class);
                    myIntent.putExtra("from_station", from_station); //Optional parameters
                    myIntent.putExtra("to_station", to_station); //Optional parameters

                    MainActivity.this.startActivity(myIntent);
                }
            }
        });

    }
}
/*String[] listAges = getResources().getStringArray(R.array.ages);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listAges);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner_age.getBackground().setColorFilter(ContextCompat.getColor(this, R.color.spinner_icon), PorterDuff.Mode.SRC_ATOP);
        spinner_age.setAdapter(dataAdapter);
        spinner_age.setSelection(0);
        spinner_age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();

                if(position > 0){
                    // get spinner value
                    Toast.makeText(parent.getContext(), "Age..." + item, Toast.LENGTH_SHORT).show();
                }else{
                    // show toast select gender
                    Toast.makeText(parent.getContext(), "none" + item, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });*/
