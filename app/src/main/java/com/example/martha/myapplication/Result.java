package com.example.martha.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ListView;

import android.widget.Toast;
import android.view.View;
import android.content.Intent;;import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Result extends AppCompatActivity {

    private TextView ticket_price;
    private TextView num_of_stations;
    private ListView listview1;
    private AdView mAdView1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);

        final Button button = findViewById(R.id.back_btn);
        button.setOnClickListener(new View.OnClickListener() {
                                      public void onClick(View v) {
                                          Intent myIntent = new Intent(Result.this, MainActivity.class);

                                          Result.this.startActivity(myIntent);
                                      }
    });


        Bundle extras = getIntent().getExtras();

        String from_station = extras.getString("from_station");
        String to_station = extras.getString("to_station");
        //calculate price
        int from_station_num = get_st_index(from_station);
        int to_station_num = get_st_index(to_station);
        String [] result = get_sts_num(from_station_num ,to_station_num );

        //End of Calculations
        TextView ticket_price = findViewById(R.id.ticket_price);

        String first = "تمن التذكرة ";
        String price = ""+get_price(Integer.parseInt(result[0]))+" جنيه";
        String second = first + price;

        ticket_price.setText(second);

        TextView num_of_stations = findViewById(R.id.num_of_stations);

        first = "عدد المحطات ";
        second = first + result[0]+" محطة";

        num_of_stations.setText(second);

        ListView listview1 = findViewById(R.id.listview1);
        String[] items;

        if(result[1].equals("") && result[2].equals("") )
            items = new String[]{from_station , to_station};

        else if(result[2].equals(""))
            items = new String[]{from_station ,result[1], to_station};

        else
            items = new String[]{from_station ,result[1] ,result[2], to_station};


        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);

        listview1.setAdapter(adapter);

        mAdView1 = (AdView) findViewById(R.id.adView1);
        AdRequest adRequest = new AdRequest.Builder()
             //   .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView1.loadAd(adRequest);

    }
    private int get_price(int num_of_sts){
        int price = 0;

        if(num_of_sts == 9 || num_of_sts < 9 ){
            price = 3;

        }

        else if(num_of_sts == 16 || num_of_sts < 16 ){
            price = 5;

        }
        else if( num_of_sts > 16  ){
            price = 7;

        }
        return price;

    }
// Shohada 1 -> [13], Shohada 2 -> [42]
// Sadat 1 -> [16] , Sadat 2 -> [45]
// Attaba 2 -> [43] , Attaba 3 -> [55]
    private int get_st_index(String st_name ){
        //create a list of items for the spinner.
        String[] stations = new String[]{

                "المرج الجديد",
                "المرج",
                "عزبة النخل",
                "عين شمس",
                "المطرية",
                "حلمية الزيتون",
                "حدايق الزيتون",
                "سراي القبة",
                "حمامات القبة",
                "كوبري القبة",
                "منشية الصدر",
                "الدمرداش",
                "غمرة",
                "الشهدا",
                "عرابي",
                "جمال عبد الناصر",
                "السادات",
                "سعد زغلول",
                "السيدة زينب",
                "الملك الصالح",
                "مارجرجس",
                "الزهراء",
                "دار السلام",
                "حدايق المعادي",
                "المعادي",
                "ثكنات المعادي",
                "طرة البلد",
                "كوتسيكا",
                "طرة الاسمنت",
                "المعصرة",
                "حدايق حلوان",
                "وادي حوف",
                "جامعة حلوان",
                "عين حلوان",
                "حلوان",

                "شبرا الخيمة",
                "كلية الزراعة",
                "المظلات",
                "الخلفاوي",
                "سانت تريز",
                "روض الفرج",
                "مسرة",
                "الشهدا",
                "العتبة",
                "محمد نجيب",
                "السادات",
                "الاوبرا",
                "الدقي",
                "البحوث",
                "جامعة القاهرة",
                "فيصل",
                "جيزة",
                "ام المصريين",
                "ساقية مكي",
                "المنيب" ,

                "العتبة" ,
                "باب الشعرية",
                "الجيش" ,
                "عبده باشا",
                "العباسية" ,
                "المعرض" ,
                "ستاد القاهرة",
                "كلية البنات" ,
                "الاهرام"};
      //  String st_name = // insert code here
        int index = -1;

        for (int i=0;i<stations.length;i++) {

            if (stations[i].equals(st_name)) {
                index = i;
                break;
            }
        }
      return index;
    }

    private String[] get_sts_num(int current_st_id , int upcoming_st_id){

        int current_st_line = get_calc_line(current_st_id);
        int upcoming_st_line = get_calc_line(upcoming_st_id);
        int switch_st_id = 0 ;
        int num_of_st = 0;
        int switch_st_index = 0;
        int switch_st2_index = 0;


        String switch_st_name = "";
        String switch_st2_name = "" ;

        if(current_st_line == upcoming_st_line )
            num_of_st =  java.lang.Math.abs(current_st_id - upcoming_st_id);
        else{

            String result[] = switch_line(current_st_line , upcoming_st_line , current_st_id , upcoming_st_id );

            num_of_st = Integer.parseInt(result[0]);
            switch_st_name= result[1];
            switch_st2_name = result[2];

        }

        // current_st_desc = create_desc_txt( switch_st_name )
        //check switch station depulication
     /*   if(current_st_name.trim() === switch_st_name.trim())
            switch_st_name = "-1";

        if(upcoming_st_name.trim() === switch_st2_name.trim())
            switch_st2_name = "-1";

        if(switch_st_name.trim() === switch_st2_name.trim()){
            switch_st_name = "-1";
            switch_st2_name = "-1";

        }*/
        String strI = "" + num_of_st;

        String []result = {strI , switch_st_name,switch_st2_name};
        return result;
    }
   private String[] switch_line(int from_line , int to_line , int current_st_id , int upcoming_st_id ){
      // int b[] ={0};
       int num_of_st = 0;

       int switch_st_id = 0;
       int switch_st2_id = 0;

       String switch_st_name = "";
       String switch_st2_name = "";
       // Shohada 1 -> [13], Shohada 2 -> [42]
// Sadat 1 -> [16] , Sadat 2 -> [45]
// Attaba 2 -> [43] , Attaba 3 -> [55]

       if(from_line == 1 && to_line == 2){
           //choose best
           if(java.lang.Math.abs(current_st_id - 16) >java.lang.Math.abs(current_st_id - 13)){
               //choose shohda
               switch_st_name = "الشهدا";
               switch_st_id = 42;
               num_of_st = java.lang.Math.abs(current_st_id - 13) + java.lang.Math.abs(upcoming_st_id - 42 );

           }
           else {
               //choose sadat
               switch_st_name = "السادات";

               switch_st_id = 45;
               num_of_st = java.lang.Math.abs(current_st_id - 16) + java.lang.Math.abs(upcoming_st_id - 45 );

           }
       }
    /*
   sadat line 1 => 16 sadat line 2 => 110
   shohada line 1 => 13 shohada line 2 => 107
   attaba line 2 => 108 attaba line 3 => 200
  */
       // Shohada 1 -> [13], Shohada 2 -> [42]
// Sadat 1 -> [16] , Sadat 2 -> [45]
// Attaba 2 -> [43] , Attaba 3 -> [55]

       if(from_line == 1 && to_line == 3){
           //choose best
           if(java.lang.Math.abs(current_st_id - 16) > java.lang.Math.abs(current_st_id - 13)){
               //choose shohda
               switch_st_name = "الشهدا";

               num_of_st = java.lang.Math.abs(current_st_id - 13);
               num_of_st = num_of_st + java.lang.Math.abs(42-43);
               num_of_st = num_of_st + java.lang.Math.abs(upcoming_st_id-55);

           }
           else {
               //choose sadat
               switch_st_name = "السادات";

               num_of_st = java.lang.Math.abs(current_st_id - 16);
               num_of_st = num_of_st + java.lang.Math.abs(45-43);
               num_of_st = num_of_st + java.lang.Math.abs(upcoming_st_id-55);

           }
           switch_st2_name = "العتبة"  ;


       }
  /*
   sadat line 1 => 16 sadat line 2 => 110
   shohada line 1 => 13 shohada line 2 => 107
   attaba line 2 => 108 attaba line 3 => 200
  */
       // Shohada 1 -> [13], Shohada 2 -> [42]
// Sadat 1 -> [16] , Sadat 2 -> [45]
// Attaba 2 -> [43] , Attaba 3 -> [55]
       if(from_line == 2 && to_line == 1){

           //choose best
           if(java.lang.Math.abs(current_st_id - 45) > java.lang.Math.abs(current_st_id - 42)){
               //choose shohda
               switch_st_name = "الشهدا";

               switch_st_id = 13;
               num_of_st = java.lang.Math.abs(current_st_id - 42) + java.lang.Math.abs(upcoming_st_id - 13 );

           }
           else {
               //choose sadat
               switch_st_name = "السادات";

               switch_st_id = 16;
               num_of_st = java.lang.Math.abs(current_st_id - 45) + java.lang.Math.abs(upcoming_st_id - 16 );

           }
       }
 /*
   sadat line 1 => 16 sadat line 2 => 110
   shohada line 1 => 13 shohada line 2 => 107
   attaba line 2 => 108 attaba line 3 => 200
  */
       // Shohada 1 -> [13], Shohada 2 -> [42]
// Sadat 1 -> [16] , Sadat 2 -> [45]
// Attaba 2 -> [43] , Attaba 3 -> [55]
       if(from_line == 2 && to_line == 3){
           switch_st_name = "العتبة";

           num_of_st = java.lang.Math.abs(current_st_id - 43) + java.lang.Math.abs(upcoming_st_id - 55 );

       }

       if(from_line == 3 && to_line == 1){
           //choose best
           switch_st_name = "العتبة"  ;

           num_of_st = java.lang.Math.abs(current_st_id-43);

           if(java.lang.Math.abs(upcoming_st_id - 16) > java.lang.Math.abs(upcoming_st_id - 13)){
               //choose shohda
               switch_st2_name = "الشهدا";

               num_of_st = num_of_st + java.lang.Math.abs(upcoming_st_id - 13);
               num_of_st = num_of_st + java.lang.Math.abs(42-43);
           }
           else {
               //choose sadat
               switch_st2_name = "السادات";

               num_of_st = num_of_st + java.lang.Math.abs(upcoming_st_id - 16);
               num_of_st = num_of_st + java.lang.Math.abs(45-43);

           }

       }
       /*
   sadat line 1 => 16 sadat line 2 => 110
   shohada line 1 => 13 shohada line 2 => 107
   attaba line 2 => 108 attaba line 3 => 200
  */
       // Shohada 1 -> [13], Shohada 2 -> [42]
// Sadat 1 -> [16] , Sadat 2 -> [45]
// Attaba 2 -> [43] , Attaba 3 -> [55]
       if(from_line == 3 && to_line == 2){
           switch_st_name = "العتبة";

           num_of_st = java.lang.Math.abs(current_st_id - 55) + java.lang.Math.abs(upcoming_st_id - 43 );

       }
       String strI = "" + num_of_st;
       String test [] = {strI ,switch_st_name , switch_st2_name};

       return test;

   }

   private int get_calc_line(int station){
        int line = 0;
        // first line -> [0 , 34]
        if ((station < 35) && (station > -1)){
            line = 1;

        }
        // second line -> [100 , 119]
        else if((station < 55) && (station > 34)){

            line = 2;

        }
        // third line -> [200 , 208]

        else if(( station > 55)){

            line = 3;
        }
        return line;
    }

}
/*
   SimpleAdapter adapter2 = new SimpleAdapter
                (this,data, android.R.layout.simple_list_item_2,
                new String[] {"title", "date"},
                new int[] {android.R.id.text1,
                        android.R.id.text2}){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                /// Get the Item from ListView
                View view = super.getView(position, convertView, parent);

                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                // Set the textda size 25 dip for ListView each item
                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP,50);

                TextView tv2 = (TextView) view.findViewById(android.R.id.text2);

                // Set the textda size 25 dip for ListView each item
                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP,12);
                // Return the view
                return view;
            }
        };
* */
